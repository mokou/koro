# koro

Koro is a small tool for helping plan events by letting people indicate their available start times. It's written
in [Svelte](https://svelte.dev), stores data in [PouchDB](https://pouchdb.com) and replicates with
[CouchDB](https://couchdb.apache.org). Times are locally displayed in the user's timezone with the help of JavaScript's
`Intl` API and [DayJS](https://day.js.org). CSS is handled with [Tailwind](https://tailwindcss.com).

## Get started

Install the dependencies...

```bash
git clone https://git.touhou.cz/mokou/koro.git
cd koro
npm install
```

...then start the Webpack dev server:

```bash
npm start
```

Navigate to [localhost:3000](http://localhost:3000). You should see your koro running. The cool thing is that you don't even
need a local database to work on the project - PouchDB works exactly like your own little in-browser CouchDB, and you
can enable syncing with your local CouchDB when you're ready.

## CSS class purging

We use Svelte's `class:classname={condition}` feature, which allows us to toggle classes based on certain circumstances.
However, this means that Tailwind's CSS purging feature will not recognize us as using these classes, and will get rid of
them. To circumvent this, we have to add all classes that we use with `class:classname` to Tailwind's whitelist located
in `tailwind.config.js`.

## Syncing with CouchDB

While this isn't required at all for development (unless you're working on something that regards the app's ability
to sync), it's pretty important when you deploy Koro. That being said, it's very easy to allow Koro to sync with an
external CouchDB instance. Simply put your connection string in the `KORO_COUCHDB_URL` environment variable. The
easiest way to do this is to create a `.env` file, which will automatically be read by Webpack. It would look like this:

```text
KORO_COUCHDB_URL=https://user:pass@mycouch.com/koro
```

...where `koro` is the name of your database. That's it! Now Koro's PouchDB will attempt to replicate with your CouchDB
instance. For security, make sure you have a user that is only allowed to access your `koro` database, and set your CouchDB
CORS settings in a restrictive way.

## Building and running in production mode

To create an optimised version of the app:

```bash
NODE_ENV=production npm run build
```

This builds all required files and puts them into `public/`, which you can then deploy to your static site hosting
service of choice.

## License

See [LICENSE](./LICENSE.md).
