const webpack = require('webpack')
const CssExtractPlugin = require('mini-css-extract-plugin')
const TerserWebpackPlugin = require('terser-webpack-plugin')
const envalid = require('envalid')
const path = require('path')
const { version } = require('./package.json')

const prod = process.env.NODE_ENV && process.env.NODE_ENV !== 'development'

const env = envalid.cleanEnv(process.env, {
  KORO_COUCHDB_URL: envalid.url({ default: 'https://localhost:5984/koro' })
})

const replacedStrings = {
  'process.browser': true,
  'process.version': JSON.stringify(version)
}

for (const name in env) {
  replacedStrings[`process.env.${name}`] = JSON.stringify(env[name])
}

module.exports = {
  name: 'bundle',
  entry: {
    main: './src/main.js'
  },
  output: {
    filename: '[name].js',
    path: path.resolve(process.cwd(), 'public/build')
  },
  mode: prod ? 'production' : 'development',
  devServer: {
    publicPath: '/build',
    contentBase: path.join(process.cwd(), 'public'),
    compress: true,
    port: 3000,
    historyApiFallback: true
  },
  devtool: !prod && 'source-map',
  optimization: prod && {
    minimizer: [new TerserWebpackPlugin({})],
    sideEffects: false
  },
  resolve: {
    alias: {
      svelte: path.resolve('node_modules', 'svelte')
    },
    extensions: ['.mjs', '.js', '.svelte'],
    mainFields: ['svelte', 'browser', 'module', 'main']
  },
  module: {
    rules: [
      {
        test: /\.(html|svelte)$/,
        exclude: /node_modules/,
        use: 'svelte-loader'
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: [
          { loader: CssExtractPlugin.loader },
          { loader: 'css-loader', options: { url: false, importLoaders: 1 } },
          { loader: 'postcss-loader' }
        ]
      }
    ]
  },
  plugins: [new webpack.DefinePlugin(replacedStrings), new CssExtractPlugin()]
}
