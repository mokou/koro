module.exports = {
  purge: {
    content: ['./src/**/*.svelte', './src/**/*.js'],
    options: {
      whitelist: [
        'bg-green-400',
        'bg-yellow-400',
        'bg-red-400',
        'bg-green-200',
        'bg-yellow-200',
        'bg-red-200',
        'bg-red-500',
        'bg-yellow-300',
        'bg-green-500',
        'bg-purple-600',
        'text-white',
        'text-gray-600',
        'time-unselectable',
        'text-purple-600',
        'dark',
        'text-gray-800',
        'border-gray-700',
        'border-gray-400'
      ]
    }
  },
  variants: {
    cursor: ['hover']
  }
}
