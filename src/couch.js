import PouchDB from 'pouchdb-browser'
import { isPouchAccessible } from './stores'
import { get } from 'svelte/store'

export async function pouch() {
  // We obtain the database, and then check if we can access it.
  const local = new PouchDB('koro', {
    auto_compaction: true
  })

  try {
    await local.info()
    return local
  } catch (e) {
    // For some reason, we cannot access the local PouchDB. This usually means
    // that IndexedDB is acting weird, and in this case, we use a online-only
    // CouchDB endpoint.
    isPouchAccessible.set(false)
    return new PouchDB(process.env.KORO_COUCHDB_URL, {
      auto_compaction: true
    })
  }
}

export async function replicateToCouch() {
  if (get(isPouchAccessible)) {
    PouchDB.replicate('koro', process.env.KORO_COUCHDB_URL, { live: false })
      .on('complete', (info) => info)
      .on('error', (err) => new Error(err))
  }
}

export function syncDoc(id) {
  if (get(isPouchAccessible)) {
    return PouchDB.sync('koro', process.env.KORO_COUCHDB_URL, {
      live: true,
      retry: true,
      doc_ids: [id]
    })
  }
}
