import { writable } from 'svelte/store'

const localStorage = window.localStorage

export const hasResponded = localStorageStore('hasResponded')
export const is24Hrs = localStorageStore('is24Hrs')
export const isDarkMode = localStorageStore('isDarkMode')
export const isPouchAccessible = writable(true)

function localStorageStore(key) {
  const item = localStorage.getItem(key)
  const { subscribe, set } = writable(JSON.parse(item) || null)

  return {
    subscribe,
    set: (value) => {
      localStorage.setItem(key, JSON.stringify(value))
      set(value)
    },
    clear: () => {
      localStorage.removeItem(key)
      set(null)
    }
  }
}
